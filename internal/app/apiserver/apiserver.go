package apiserver

import (
	"fmt"
	"io"
	"log"
	"net/http"

	"bitbucket.org/sichkar_andrii/spotify-bot/internal/app/model"
	"bitbucket.org/sichkar_andrii/spotify-bot/internal/app/store"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)


type APIServer struct {
	config *Config
	logger *logrus.Logger
	router *mux.Router
	store *store.Store
}

func New(config *Config) *APIServer {
	return &APIServer{
		config: config,
		logger: logrus.New(),
		router: mux.NewRouter(),
	}
}

func (s *APIServer) Start() error {
	if err := s.configureLogger(); err != nil {
		return err
	}

	s.configureRouter()

	if err:= s.configureStore(); err != nil {
		return err
	}

	s.logger.Info("starting api server")

	return http.ListenAndServe(s.config.BindAddr, s.router)
}

func (s *APIServer) configureLogger() error {
	level, err := logrus.ParseLevel(s.config.LogLevel)
	if err != nil {
		return nil
	}

	s.logger.SetLevel(level)

	return nil
}

func (s *APIServer) configureRouter() {
	s.router.HandleFunc("/hello", s.handleHello())
	s.router.HandleFunc("/make", s.makeUser(&store.Store{}))
}

func (s *APIServer) makeUser(t *store.Store) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("lol")
	
	u,err := t.User().Create(&model.User{
		Email: "wasderr@example",
		EncryptedPassword: "qwessqweqwe",
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(u.Email)
	}
	
}

func (s *APIServer) configureStore() error {
	st := store.New(s.config.Store)
	if err:= st.Open(); err != nil {
		return err
	}

	s.store = st

	return nil
}

func (s *APIServer) handleHello() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Hello")
	}
}