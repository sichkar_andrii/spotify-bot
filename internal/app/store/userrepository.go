package store

import (
  "bitbucket.org/sichkar_andrii/spotify-bot/internal/app/model"
)


type UserRepository struct {
  store *Store
}

func (r *UserRepository) Create(u *model.User) (*model.User, error) {
  var store Store
  sqlStatement := `INSERT INTO users (email, encrypted_password) VALUES ($1, $2)`
  conn := store.OpenConnection()
  conn.QueryRow(sqlStatement, u.Email, u.EncryptedPassword)
  
  defer conn.Close()
  return u, nil
}

func (r *UserRepository) FindByEmail(email string) (*model.User, error) {
  u := &model.User{}
  var store Store
  sqlStatement := `SELECT id, email, encrypted_password FROM users WHERE email = $1`
  conn := store.OpenConnection()
  conn.QueryRow(sqlStatement, u.Email).Scan(
    &u.ID,
    &u.Email,
    &u.EncryptedPassword,
  )

  return u, nil
}