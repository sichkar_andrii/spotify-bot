package store_test

import (
	"testing"

	"bitbucket.org/sichkar_andrii/spotify-bot/internal/app/model"
	"bitbucket.org/sichkar_andrii/spotify-bot/internal/app/store"
	"github.com/stretchr/testify/assert"
)


func TestUserRepository_Create(t *testing.T) {
	s, teardown := store.TestStore(t, databaseURL)
	defer teardown("users")

	u,err := s.User().Create(&model.User{
		Email: "user@example",
	})
	assert.NoError(t, err)
	assert.NotNil(t, u)
}

func TestUserRepository_FindByEmail(t *testing.T) {
	s, teardown := store.TestStore(t, databaseURL)
	defer teardown("users")

	email := "user@example"
	_,err := s.User().FindByEmail(email)
	assert.Error(t, err)

	s.User().Create(&model.User{
		Email: email,
	})
	u,err := s.User().FindByEmail(email)
	assert.NoError(t, err)
	assert.NotNil(t, u)
}